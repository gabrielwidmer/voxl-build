#!/bin/bash
################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

# run this INSIDE the docker image

set -e #exit on error

echo "[INFO] build stage started"

usr=$(whoami)
echo "[INFO] running as user: ${usr}"
echo "[INFO] current working directory:"
pwd

if [ $usr = "root" ] ; then
    echo "[ERROR] This script should be run as a non-root user"
    exit 1
fi

# Manifest for CAF repos
REPO_MANIFEST_FILE="LE.UM.1.3.r4-06300-8x96.0.xml"
REPO_MANIFEST_URL="git://codeaurora.org/quic/le/le/manifest.git"
DEFAULT_BUILD_DIR=/home/user/workspace/

# use default build directory unless user specifie another
if [ "$#" -ne 1 ]; then
    echo "[INFO] Using default build directory: "$DEFAULT_BUILD_DIR
    mkdir -p $DEFAULT_BUILD_DIR
    cd $DEFAULT_BUILD_DIR
else
    echo "[INFO] Using provided build directory: "$1
    mkdir -p $1
    cd $1
    set -- "" # unset to prevent issues with source calls later
fi

workspace=`pwd`
echo "[INFO] Workspace: "$workspace
pokydir=$workspace/poky
echo "[INFO] Poky directory: "$pokydir

# prevent git from prompting us such that this requires no user intervention
git config --global color.ui false
git config --global user.name foo
git config --global user.email foo@bar.com

# Check git config. repo init fails if the following two aren't set
git config --get user.name > /dev/null  || { echo "[ERROR] Use git config --global user.name before running this script"; exit 2; }
git config --get user.email > /dev/null || { echo "[ERROR] Use git config --global user.email before running this script"; exit 2; }

# Now get the open source code from CAF
repo init -u ${REPO_MANIFEST_URL} -b release -m ${REPO_MANIFEST_FILE}
repo sync -c --no-tags -q -j32

#
# Clone the 'meta-voxl' layer into the poky directory. This contains the open 
# source patches for the kernel. This layer gets automatically added to 
# bblayers.conf later in this script.
#
cd $pokydir
git clone https://gitlab.com/voxl-public/meta-voxl.git

cd $workspace

# We need to add a missing bbclass. Since we don't really know what qlicense.bbclass
# contains, we'll just assume it's a generic CLOSED license
echo "LICENSE = \"CLOSED\"" >> poky/meta-qti-bsp/classes/qlicense.bbclass

# Now we're ready to configure the build environment
cd $pokydir

# This script, amongst other operations, dynamically configures bblayers.conf, adding the layers above
source build/conf/set_bb_env.sh > /dev/null

export MACHINE=apq8096
export PRODUCT=drone
export DISTRO=msm-perf

# compile (grab a coffee)
echo "[INFO] creating boot image:"
bitbake linux-quic

echo "[INFO] boot image created:"
echo "[INFO] $workspace/poky/build/tmp-glibc/deploy/images/apq8096-drone/apq8096-boot.img"

# build wlan.ko (per RA's instructions)
echo "[INFO] creating wlan.ko module"
bitbake qcacld-ll

echo "[INFO] module created:"
echo "[INFO] $workspace/poky/build/tmp-glibc/deploy/images/apq8096-drone/kernel_modules/qcacld-ll/wlan.ko"

echo "[INFO] build stage complete!"
