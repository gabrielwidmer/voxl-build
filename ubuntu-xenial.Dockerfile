################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

################################################################################
# Dockerfile for building an image that can be used to build the VOXL open
# source Kernel.
# Author: Rahul Anand
################################################################################

FROM ubuntu:xenial
ENV DEBIAN_FRONTEND noninteractive

# Add repo for the right gcc
RUN apt-get update \
   && apt-get -y --quiet --no-install-recommends install software-properties-common

RUN add-apt-repository ppa:ubuntu-toolchain-r/test

# Now install the packages
RUN apt-get update \
   && apt-get -y --quiet --no-install-recommends install \
      gcc-4.8-multilib \
      g++-4.8-multilib \
      gawk \
      wget \
      git-core \
      diffstat \
      unzip \
      texinfo \
      gcc-multilib \
      build-essential \
      chrpath \
      bzip2 \
      zip \
      cpio \
      bc \
      make \
      autoconf \
      automake \
      libtool \
      wget \
      curl \
      sudo \
      libxml-simple-perl \
      doxygen \
      qemu-user-static \
      android-tools-fsutils \
      debootstrap \
      schroot \
      vim \
      less \
      iputils-ping \
      python \
      python-pip \
      python-setuptools \
      python-tk \
   && apt-get clean autoclean \
   && pip install --upgrade pip \
   && curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/bin/repo \
   && chmod a+x /usr/bin/repo \
   && echo "dash dash/sh boolean false" | debconf-set-selections \
   && DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash

# Add a non-root user with the following details:
# username: user
# password: user
# Home dir: /home/user
# Groups   : sudo, user

RUN /usr/sbin/useradd -m -s /bin/bash -G sudo user \
  && echo user:user | /usr/sbin/chpasswd

# add the build script
ADD build.sh /home/user

# set defualt user to user when launch docker
USER user
